# MAC0110-MiniEP7

	This is a small college work by a student in his first semester of Bachelor of Computer Science from the University of São Paulo.
	The objective of this work is to learn more (although still superficially) about the branch, merge and remote mechanics of Git using the web-based tool GitLab. There are some algorithms exercises to be solved, but they are only the means to learn those Git concepts, as they are not the focus of this work.
