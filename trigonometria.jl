# MAC0110 - MiniEP7
# David de Barros Tadokoro - 10300507

function quaseigual(v1, v2)
	erro = 0.0001
	igual = abs(v1 - v2)
	if igual <= erro
		return true
	else
		return false
	end
end

#SINE

function sine(x)
	taylor_sin = 0
	for i in 0:9
		taylor_sin = taylor_sin + (((-1)^i)/factorial(big(2*i + 1))) * (x^(2*i + 1))
	end
	return taylor_sin
end

function check_sin(value, x)
	return quaseigual(value, sin(x))
end

#COSINE

function cosine(x)
	taylor_cos = 0
	for i in 0:9
		taylor_cos = taylor_cos + (((-1)^i)/factorial(big(2*i))) * (x^(2*i))
	end
	return taylor_cos
end

function check_cos(value, x)
	return quaseigual(value, cos(x))
end

#TANGENT

function bernoulli(n)
	n *= 2
	A = Vector{Rational{BigInt}}(undef, n + 1)
	for m = 0 : n
		A[m + 1] = 1 // (m + 1)
		for j = m : -1 : 1
			A[j] = j * (A[j] - A[j + 1])
		end
	end
	return abs(A[1])
end

function tangent(x)
	taylor_tan = 0
	for i in 1:10
		taylor_tan = taylor_tan + ((bernoulli(i) * 2^(2*i) * (2^(2*i) - 1))/factorial(big(2*i))) * (x^(2*i -1))
	end
	return taylor_tan
end

function check_tan(value, x)
	return quaseigual(value, tan(x))
end

function taylor_sin(x)
	taylor_sin = 0
	for i in 0:9
		taylor_sin = taylor_sin + (((-1)^i)/factorial(big(2*i + 1))) * (x^(2*i + 1))
	end
	return taylor_sin
end

function taylor_cos(x)
	taylor_cos = 0
	for i in 0:9
		taylor_cos = taylor_cos + (((-1)^i)/factorial(big(2*i))) * (x^(2*i))
	end
	return taylor_cos
end

function taylor_tan(x)
	taylor_tan = 0
	for i in 1:10
		taylor_tan = taylor_tan + ((bernoulli(i) * 2^(2*i) * (2^(2*i) - 1))/factorial(big(2*i))) * (x^(2*i -1))
	end
	return taylor_tan

function test()
	# parte 1
	@test quaseigual(sine(1), 0.8414709848078965)
	@test quaseigual(sine(pi/2), 1.0)
	@test quaseigual(cosine(1), 0.5403023058681398)
	@test quaseigual(cosine(pi/2), 0.0)
	@test quaseigual(tangent(pi/4), 1.0)
	@test quaseigual(tangent(pi/6), 0.5773502691896257)
	println("Fim dos testes para a Parte 1")
	# parte 2
	@test check_sin(0.5, pi/6)
	@test check_sin(sqrt(3)/2, pi/3)
	@test check_cos(sqrt(2)/2, pi/4)
	@test check_cos(1.0, 0.0)
	@test check_tan(1.0, pi/4)
	@test check_tan(0.0, pi)
	@test !check_sin(42, pi)
	@test !check_cos(42, pi)
	@test !check_tan(42, pi/4)
	println("Fim dos testes para a Parte 2")
end
